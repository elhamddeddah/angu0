import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { MatchsComponent } from './matchs/matchs.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { ResultsComponent } from './results/results.component';
import { AuthGuardService } from './services/auth-guard.service';

const appRoutes: Routes = [
  { path: 'matchs', canActivate: [AuthGuardService], component: MatchsComponent },
  { path: 'results', canActivate: [AuthGuardService], component: ResultsComponent },
  { path: 'auth', component: AuthComponent },
  { path: '',  canActivate: [AuthGuardService], component: ResultsComponent},
  { path: 'not-found', component: NotfoundComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
