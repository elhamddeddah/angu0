import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthModel } from 'src/model/auth.model';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent   {
  title = 'Groupfixe';

  authModel:AuthModel=new AuthModel();
  subscription!: Subscription;
  constructor(private authService: AuthService){
    this.subscription=authService.subject.subscribe(
      (response: AuthModel) => {
        this.authModel=response
       // this.SpinnerService.hide();
      /*  this.authModel.isAuth=response.isAuth;
       this.authModel.username=response.username;
       this.authModel.phone=response.phone; */
      }
    );
    this.authService.emitSubject();
  }

}
