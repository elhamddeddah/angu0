import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  authStatus!: boolean;
  date: string;

  constructor(private authService: AuthService,private router: Router) {
    this.date = new Date().toISOString().slice(0, 16);
   }

  ngOnInit(): void {
    this.authStatus = this.authService.authModel.isAuth;
  }


  onSubmit(form: NgForm) {
  const code = form.value['code'];
  console.log(code)
  this.authService.login(code);
  }
  onSignOut() {
    this.authService.signOut();
    this.authStatus = this.authService.authModel.isAuth;
  }

}
