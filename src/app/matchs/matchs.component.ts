import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { matchModel } from 'src/model/match.model';
import { MatchsService } from '../services/matchs.service';

@Component({
  selector: 'app-matchs',
  templateUrl: './matchs.component.html',
  styleUrls: ['./matchs.component.scss']
})
export class MatchsComponent implements OnInit,OnDestroy {

  matchs:  Array<matchModel>=[];
  subscription!: Subscription;

  constructor( private matchsService: MatchsService) { }

  ngOnInit(): void {
     this.matchsService.getMatchs();
     this.subscription=this.matchsService.subject.subscribe(
      (response: Array<matchModel>) => {
        this.matchs=response;
        response.forEach(element => {
          console.log(element.date);
          
        });
      }
    );
    this.matchsService.emitSubject();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
   }





}
