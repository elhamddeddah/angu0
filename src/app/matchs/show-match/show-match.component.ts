import { Component, Input, OnInit } from '@angular/core';
import { matchModel } from 'src/model/match.model';

@Component({
  selector: 'app-show-match',
  templateUrl: './show-match.component.html',
  styleUrls: ['./show-match.component.scss']
})
export class ShowMatchComponent implements OnInit {

@Input() match!:matchModel;
  constructor() { }

  ngOnInit(): void {
    this.match.getdayAr();
    
  }

}
