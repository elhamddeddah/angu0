import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AuthModel } from 'src/model/auth.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authModel:AuthModel=new AuthModel();

  subject = new Subject<any>();
  emitSubject() {
    this.subject.next(this.authModel);
  }

  constructor(private httpClient: HttpClient,private router: Router) {

   }
  signOut() {
    this.authModel.isAuth = false;
  }
  login(code:string){
    this.httpClient
    .get<any>('https://angular-81916.firebaseio.com/users/'+code+'.json')
    .subscribe(
      (response) => {
        if(response!=null){
          console.log(response)
          this.authModel.isAuth=true;
          this.authModel.username=response.name;
          this.authModel.phone=response.phone;
          this.router.navigate(['/matchs']);
          this.emitSubject();
          return true;
        }

        else{
          console.log(response)
          return false;
        }

      },
      (error) => {
        return false;
        console.log('Erreur ! : ' + error);
      }
    );
  }
}
