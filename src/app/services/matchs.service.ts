import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { matchModel } from 'src/model/match.model';

@Injectable({
  providedIn: 'root',
})
export class MatchsService {
  matchs:  Array<matchModel>=[];
 
  subject = new Subject<any>();
  constructor(private httpClient: HttpClient) {
   
  }

  emitSubject() {
    this.subject.next(this.matchs);
  }
  getMatchs(){
    let response:any={
      "-MtmA42GiZXCRA8pzBWM": {
          "by": "ََأحمد دن",
          "date": "2022-01-16",
          "hour": "20",
          "place": "النصرة",
          "status": "0"
   },
      "-MtmA8o5_cnfB4Cbo-nU": {
          "by": "حم امهال",
          "date": "2022-01-10",
          "hour": "21",
          "place": "الاستتقال ",
          "status": "1"
      },
      "-MtmCTrMuvv8pRlSAxQN": {
          "by": "دداه",
          "date": "2022-01-19",
          "hour": "21",
          "place": "النصرة",
          "status": "3" 
      }
    }
    const objectArray = Object.entries(response);
          objectArray.forEach(([key, value]) => {
            var  match:matchModel= new matchModel();
            match.copy(value)
            match.id=key;
            this.matchs.push(match)
          });
          this.emitSubject();
 /* this.httpClient
      .get<any>('https://angular-81916.firebaseio.com/matchs.json')
      .subscribe(
        (response) => {
          const objectArray = Object.entries(response);
          objectArray.forEach(([key, value]) => {
            console.log(key);
            this.match=<matchModel> value;
            this.match.id=key;
            console.log(this.match)
            this.matchs.push(this.match)
          });
       console.log(this.matchs.toLocaleString);
          this.emitSubject();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      ); */
  }
}
